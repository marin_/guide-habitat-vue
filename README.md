# Guide de l'Habitat

L'application Guide de l'Habitat, développé avec Vue.js en conjonction avec une api développée sous Laravel.

## Liens des tutos suivis :

  https://guillaumebriday.fr/laravel-vuejs-faire-une-todo-list-partie-5-authentification-avec-vuex-et-vue-router
  https://medium.com/front-end-hacking/persisting-user-authentication-with-vuex-in-vue-b1514d5d3278
